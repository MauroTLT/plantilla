<?php

namespace plantilla\app\entity;

use plantilla\core\database\IEntity;

class Usuario implements IEntity
{
	private $id;
	private $username;
	private $password;
	private $role;

	function __construct($username='', $password='', $role='')
	{
		$this->id = null;
		$this->username = $username;
		$this->password = $password;
		$this->role = $role;
	}

	function getId() {
		return $this->id;
	}

	function getUsername() {
		return $this->username;
	}

	function getPassword() {
		return $this->password;
	}

	function getRole() {
		return $this->role;
	}

	public function toArray($value='')
	{
		return ['id' => $this->getId(), 'username' => $this->getUsername(), 'password' => $this->getPassword(), 'role' => $this->getRole()];
	}
}