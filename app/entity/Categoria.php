<?php

namespace plantilla\app\entity;

use plantilla\core\database\IEntity;

class Categoria implements IEntity
{
	private $id;
	private $nombre;
	private $numImagenes;

	public function __construct($nombre='', $numImagenes=0)
	{
		$this->id = null;
		$this->nombre = $nombre;
		$this->numImagenes = $numImagenes;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getNombre()
	{
		return $this->nombre;
	}

	public function getNumImagenes()
	{
		return $this->numImagenes;
	}

	public function setNumImagenes($numImagenes)
	{
		$this->numImagenes = $numImagenes;
	}

	public function toArray()
	{
		return ['id' => $this->getId(), 'nombre' => $this->getNombre(), 'numImagenes' => $this->getNumImagenes()];
	}
}