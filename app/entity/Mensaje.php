<?php

namespace plantilla\app\entity;

use plantilla\core\database\IEntity;

class Mensaje implements IEntity {

    private $id;
    private $nombre;
    private $apellidos;
    private $asunto;
    private $email;
    private $texto;
    private $validado;
    private $fecha;

    public function __construct($nombre="", $apellidos="", $asunto="", $email="", $texto="") {
        $this->id = null;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->asunto = $asunto;
        $this->email = $email;
        $this->texto = $texto;
        $this->validado = 0;
        $this->fecha = null;
    }

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function getAsunto() {
        return $this->asunto;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getValidado() {
        return $this->validado;
    }

    public function setValidado($validado) {
        $this->validado = $validado;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function toArray()
    {
       return ['id' => $this->getId(), 'nombre' => $this->getNombre(), 'apellidos' => $this->getApellidos(), 'asunto' => $this->getAsunto(), 'email' => $this->getEmail(), 'texto' => $this->getTexto(), 'validado' => $this->getValidado(), 'fecha' => $this->getFecha()];
    }

}
