<?php

namespace plantilla\app\entity;

use plantilla\core\database\IEntity;

class ImagenGaleria implements IEntity {

    const RUTA_IMATGES_PORTFOLIO = "images/index/portfolio/";
    const RUTA_IMATGES_GALERIA = "images/index/gallery/";

    private $id;
    private $nom;
    private $descripcio;
    private $numVisualitzacions;
    private $numLikes;
    private $numBaixades;
    private $categoria;

    public function __construct($nom = "", $descripcio = "", $categoria = 0, $numVisualitzacions = 0, $numLikes = 0, $numBaixades = 0){
        $this->id = null;
        $this->nom = $nom;
        $this->descripcio = $descripcio;
        $this->numVisualitzacions = $numVisualitzacions;
        $this->numLikes = $numLikes;
        $this->numBaixades = $numBaixades;
        $this->categoria = $categoria;
    }

    public function __toString() {
        return $this->getDescripcio();
    }

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function getDescripcio() {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio) {
        $this->nom = $descripcio;
    }

    public function getNumVisualitzacions() {
        return $this->numVisualitzacions;
    }

    public function setNumVisualitzacions($numVisualitzacions) {
        $this->nom = $numVisualitzacions;
    }

    public function getNumLikes() {
        return $this->numLikes;
    }

    public function setNumLikes($numLikes) {
        $this->nom = $numLikes;
    }

    public function getNumBaixades() {
        return $this->numBaixades;
    }

    public function setNumBaixades($numBaixades) {
        $this->nom = $numBaixades;
    }

    public function getCategoria() {
        return $this->categoria;
    }

    public function getUrlPortfolio() {
        return self::RUTA_IMATGES_PORTFOLIO.$this->getNom();
    } 

    public function getUrlGallery() {
        return self::RUTA_IMATGES_GALERIA.$this->getNom();
    }

    public function toArray()
    {
       return ['id' => $this->getId(), 'nom' => $this->getNom(), 'descripcio' => $this->getDescripcio(), 'numVisualitzacions' => $this->getNumVisualitzacions(), 'numLikes' => $this->getNumLikes(), 'numBaixades' => $this->getNumBaixades(), 'categoria' => $this->getCategoria()];
    }
}
