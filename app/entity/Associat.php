<?php

namespace plantilla\app\entity;

use plantilla\core\database\IEntity;

class Associat implements IEntity {

    const RUTA_IMAGES_LOGOS = "images/associats/";

    private $id;
    private $nom;
    private $logo;
    private $descripcio;

    public function __construct($nom="", $logo="", $descripcio=""){
        $this->id = null;
        $this->nom = $nom;
        $this->logo = $logo;
        $this->descripcio = $descripcio;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function getLogo() {
        return $this->logo;
    }

    public function setLogo($logo) {
        $this->logo = $logo;
    }

    public function getDescripcio() {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio) {
        $this->nom = $descripcio;
    }

    public function getUrlLogo() {
        return self::RUTA_IMAGES_LOGOS.$this->getLogo();
    }

    public function toArray()
    {
       return ['id' => $this->getId(), 'nom' => $this->getNom(), 'logo' => $this->getLogo(), 'descripcio' => $this->getDescripcio()];
    }

}
