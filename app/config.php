<?php

use Monolog\Logger;

return [
	'database' => [
		'name' => 'cursphp7',
		'user' => 'root',
		'password' => '1234',
		'connection' => 'mysql:host=mysql',
		'options' => [PDO::MYSQL_ATTR_INIT_COMMAND => "Set NAMES utf8", PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT => true]
	],
	'logs' => [
		'filename' => 'curso.log',
		'level' => Logger::WARNING
	],
	'routes' => [
		'filename' => 'routes.php'
	],
	'project' => [
		'namespace' => 'plantilla'
	],
	'swiftmail' => [
		'smtp_server' => 'smtp.gmail.com',
		'smtp_port' => 587,
		'smtp_security' => 'tls',
		'username' => 'mauroo.inf1@gmail.com',
		'password' => 'granContraseña',
		'email' => 'mauroo.inf1@gmail.com',
		'name' => 'Pepe la Rana'
	],
	'security' => [
		'roles' => [
			'ROLE_ADMIN' => 3,
			'ROLE_USER' => 2,
			'ROLE_ANONYMOUS' => 1
		]
	]
];