<?php

namespace plantilla\app\repository;

use plantilla\app\entity\Categoria;
use plantilla\core\database\QueryBuilder;

class CategoriaRepository extends QueryBuilder
{
	public function __construct($table='categorias', $classEntity=Categoria::class)
	{
		parent::__construct($table, $classEntity);
	}

	public function nuevaImagen($categoria)
	{
		$categoria->setNumImagenes($categoria->getNumImagenes()+1);

		$this->update($categoria);
	}
}