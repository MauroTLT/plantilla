<?php

namespace plantilla\app\repository;

use plantilla\core\database\QueryBuilder;
use plantilla\app\entity\ImagenGaleria;
use plantilla\app\repository\CategoriaRepository;

class ImagenGaleriaRepository extends QueryBuilder
{
	public function __construct($table='imagenes', $classEntity=ImagenGaleria::class)
	{
		parent::__construct($table, $classEntity);
	}

	public function getCategoria($imagenGaleria)
	{
		$categoriaRepository = new CategoriaRepository();

		return $categoriaRepository->find($imagenGaleria->getCategoria());
	}

	public function guarda($imagenGaleria)
	{
		$fnGuardaImagen = function () use ($imagenGaleria)
		{
			$categoria = $this->getCategoria($imagenGaleria);
			$categoriaRepository = new CategoriaRepository();
			$categoriaRepository->nuevaImagen($categoria);
			$this->save($imagenGaleria);
		};
		$this->executeTransaction($fnGuardaImagen);
	}
}