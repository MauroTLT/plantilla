<?php

namespace plantilla\app\repository;

use plantilla\core\database\QueryBuilder;
use plantilla\app\entity\Usuario;

class UsuarioRepository extends QueryBuilder
{
	public function __construct($table='usuarios', $classEntity=Usuario::class)
	{
		parent::__construct($table, $classEntity);
	}

	public function guarda($usuario)
	{
		$fnGuardaUsuario = function () use ($usuario)
		{
			$this->save($usuario);
		};
		$this->executeTransaction($fnGuardaUsuario);
	}
}