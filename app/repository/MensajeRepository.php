<?php

namespace plantilla\app\repository;

use plantilla\app\entity\Mensaje;
use plantilla\core\database\QueryBuilder;

class MensajeRepository extends QueryBuilder
{
	public function __construct($table='mensajes', $classEntity=Mensaje::class)
	{
		parent::__construct($table, $classEntity);
	}

	public function guarda($mensaje)
	{
		$fnGuardaMensaje = function () use ($mensaje)
		{
			$this->save($mensaje);
		};
		$this->executeTransaction($fnGuardaMensaje);
	}
}