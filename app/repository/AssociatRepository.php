<?php

namespace plantilla\app\repository;

use plantilla\app\entity\Associat;
use plantilla\core\database\QueryBuilder;

class AssociatRepository extends QueryBuilder
{
	public function __construct($table='asociados', $classEntity=Associat::class)
	{
		parent::__construct($table, $classEntity);
	}

	public function guarda($asociado)
	{
		$fnGuardaAsociado = function () use ($asociado)
		{
			$this->save($asociado);
		};
		$this->executeTransaction($fnGuardaAsociado);
	}
}