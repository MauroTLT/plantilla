
<h1 align='center'>Lista de Asociados</h1>
<table class="table" border="1" align="center" width="700px">
	<thead>
		<tr align="center">
			<th scope="col">#</th>
			<th scope="col">Imagen</th>
			<th scope="col">Nombre</th>
			<th scope="col">Descripción</th>
		</tr>
	</thead>
	<tbody>
<?php foreach (($imagenes ?? []) as $imagen) { ?>
		<tr align="center">
			<th scope="row"><?= $imagen->getId() ?></th>
			<td><img src="<?= $imagen->getUrlLogo()?>" alt="<?= $imagen->getDescripcio()?>" title="<?= $imagen->getDescripcio()?>" width="100px"/></td>
			<td><?= $imagen->getNom()?></td>
			<td><?= $imagen->getDescripcio()?></td>
		</tr>
<?php } ?>
	</tbody>
</table>