
<!-- Principal Content Start -->
<div id="galeria">
	<div class="container">
		<div class="row pull-left">
			<div class="col-xs-12 col-sm-8 col-md-8 col-sm-push-2">
				<h1>IMAGEN GALERÍA</h1>
				<hr>
				<div class="imagenes_galeria">
					<img width="600" height="600" src="/<?= $imagen->getUrlGallery()?>" alt="<?= $imagen->getDescripcio()?>" title="<?= $imagen->getDescripcio()?>"/>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 pull-right">
			<h1 align="center">FILTROS</h1>
			<hr>
			<form action="/imagenes-galeria/<?= $imagen->getId()?>" method="GET">
			<?php foreach ($filtros as $nombre => $value) { ?>
					<h4 align="center">
						<label class="form-label"><?= $nombre ?></label>
						<input type="checkbox" class="pull-right" style="transform: scale(2.5);" name="efecto[]" value="<?= $nombre ?>">
					</h4>
					<hr>
			<?php } ?>
				<button type="submit" class="btn btn-primary btn-block btn-lg">APLICAR FILTROS</button>
			</form><br>
		</div>
		<h1><?= $filtro ?></h1>
		<br><br><br>
	</div>
</div>
<!-- Principal Content Start -->
