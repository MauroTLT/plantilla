	<!-- Navigation Bar -->
	<nav class="navbar navbar-fixed-top navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#menu">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top"> <span>[PHOTO]</span>
				</a>
			</div>
			<div class="collapse navbar-collapse navbar-right" id="menu">
				<ul class="nav navbar-nav">
					<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('/');?>"><a href="/"><i class="fa fa-home sr-icons"></i> Home</a></li>
					<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('about');?>"><a href="/about"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
					<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('blog');?>"><a href="/blog"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
					<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('contact');?>"><a href="/contact"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
					<?php if (is_null($app['user'])) : ?>
						<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('login');?>"><a href="/login"><i class="fa fa-user-secret sr-icons"></i> Login</a></li>
						<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('register');?>"><a href="/register"><i class="fa fa-registered sr-icons"></i> Register</a></li>
					<?php else : ?>
						<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('galeria');?>"><a href="/imagenes-galeria"><i class="fa fa-image sr-icons"></i> Galeria</a></li>
						<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('associats');?>"><a href="/associats"><i class="fa fa-hand-o-right sr-icons"></i> Asociados</a></li>

						<li class="<?php echo plantilla\app\utils\Utils::opcionActiva('logout');?>"><a href="/logout"><i class="fa fa-sign-out sr-icons"></i> <?= $app['user']->getUsername() ?> - Salir</a></li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</nav>
	<!-- End of Navigation Bar -->