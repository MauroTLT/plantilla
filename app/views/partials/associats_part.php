
<!-- Box within partners name and logo -->
<div class="last-box row">
	<div class="col-xs-12 col-sm-4 col-sm-push-4 last-block">
		<div class="partner-box text-center">
			<p>
				<i class="fa fa-map-marker fa-2x sr-icons"></i> <span
					class="text-muted">35 North Drive, Adroukpape, PY 88105,
					Agoe Telessou</span>
			</p>
			<h4>Our Main Partners</h4>
			<hr>
			<div class="text-muted text-left">
				<ul class="list-inline">
					<li><img width="75" src="<?php echo $asociados[0]->getUrlLogo(); ?>" alt="<?php echo $asociados[0]->getDescripcio(); ?>"></li>
					<li><?php echo $asociados[0]->getNom(); ?></li>
				</ul>
				<ul class="list-inline">
					<li><img width="75" src="<?php echo $asociados[1]->getUrlLogo(); ?>" alt="<?php echo $asociados[1]->getDescripcio(); ?>"></li>
					<li><?php echo $asociados[1]->getNom(); ?></li>
				</ul>
				<ul class="list-inline">
					<li><img width="75" src="<?php echo $asociados[2]->getUrlLogo(); ?>" alt="<?php echo $asociados[2]->getDescripcio(); ?>"></li>
					<li><?php echo $asociados[2]->getNom(); ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- End of Box within partners name and logo -->