
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>ASOCIADO</h1>
            <hr>

            <div class="asociado">
                <div class="row">
                	<div class="col-md-4">
                		<h2>IMAGEN</h2>
                		<hr>
                		<img src="/<?= $asociado->getUrlLogo()?>" alt="<?= $asociado->getDescripcio()?>" title="<?= $asociado->getDescripcio()?>" width="100px"/></td>
                	</div>
                	<div class="col-md-4">
                		<h2>NOMBRE</h2>
                		<hr>
                		<h3><?= $asociado->getNom() ?></h3>
                	</div>
                	<div class="col-md-4">
                		<h2>DESCRIPCIÓN</h2>
                		<hr>
                		<h3><?= $asociado->getDescripcio() ?></h3>
                	</div>
                </div>
                <br><br>
                <h1>MODIFICAR ASOCIADO</h1>
                <form class="form-horizontal" action="/associats/modify" method="post" enctype="multipart/form-data">
                	<input type="hidden" name="id" value="<?= $asociado->getId() ?>" />
	                <div class="form-group">
	                    <div class="col-xs-12">
	                        <label class="label-control">Nombre</label>
	                        <input class="form-control" type="text" name="nombre" value="" required>
	                    </div>
	                </div>
	                <div class="form-group">
	                    <div class="col-xs-12">
	                        <label class="label-control">Imagen</label>
	                        <input class="form-control-file" type="file" name="imagen">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <div class="col-xs-12">
	                        <label class="label-control">Descripción</label>
	                        <textarea class="form-control" name="descripcion"></textarea>
	                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
	                    </div>
	                </div>
	            </form>
            </div>
        </div>
    </div>
</div>
<!-- Principal Content Start -->
