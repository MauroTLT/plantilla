
<div id="asociados">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>ASOCIADOS</h1>
            <hr>
            
            <?php include __DIR__.'/partials/show-error-part.php'; ?>

            <form class="form-horizontal" action="/nuevo-associat/nuevo" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Nombre</label>
                        <input class="form-control" type="text" name="nombre" value="<?= $nombre ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" type="file" name="imagen">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion"><?= $descripcion ?></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
            <hr class="divider">
            <div class="imagenes_asociados">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Imagen</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripción</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach (($imagenes ?? []) as $imagen) { ?>
                        <tr>
                            <th scope="row"><?= $imagen->getId() ?></th>
                            <td><img src="<?= $imagen->getUrlLogo()?>" alt="<?= $imagen->getDescripcio()?>" title="<?= $imagen->getDescripcio()?>" width="100px"/></td>
                            <td><?= $imagen->getNom()?></td>
                            <td><?= $imagen->getDescripcio()?></td>
                            <td>
                                <a href="/associats/<?= $imagen->getId() ?>"><button type="submit" class="btn btn-warning btn-lg">MODIFICAR</button></a>
                            </td>
                            <td>
                                <form action="/associats/borrar" method="POST">
                                    <input type="hidden" name="id" value="<?= $imagen->getId() ?>" />
                                    <button type="submit" class="btn btn-danger btn-lg">BORRAR</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php if (plantilla\core\Security::isUserGranted('ROLE_ADMIN') === true) {?>
            <br><br><br>
            <div class="row">
                <div class="col-md-12">
                    <form action="/associats/imprimir" method="POST">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">IMPRIMIR</button>
                    </form>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>
<br><br><br>
<!-- Principal Content Start -->