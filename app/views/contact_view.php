
	<!-- Principal Content Start -->
	<div id="contact">
		<div class="container">
			<div class="col-xs-12 col-sm-8 col-sm-push-2">
				<?php include __DIR__.'/partials/show-error-part.php'; ?>
				<h1>CONTACT US</h1>
				<hr>
				<p>Aut eaque, laboriosam veritatis, quos non quis ad
					perspiciatis, totam corporis ea, alias ut unde.</p>
				<form action="/contact/mensaje" method="POST" class="form-horizontal">
					<div class="form-group">
						<div class="col-xs-6">
							<label class="label-control">First Name</label> <input
								class="form-control" type="text" name="nombre" value="<?php echo(isset($_POST['enviar']) && !empty($errores) ? $nombre : ""); ?>">
						</div>
						<div class="col-xs-6">
							<label class="label-control">Last Name</label> <input
								class="form-control" type="text" name="apellido" value="<?php echo(isset($_POST['enviar']) && !empty($errores) ? $apellido : ""); ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label class="label-control">Email</label> <input
								class="form-control" type="text" name="email" value="<?php echo(isset($_POST['enviar']) && !empty($errores) ? $email : ""); ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label class="label-control">Subject</label> <input
								class="form-control" type="text" name="asunto" value="<?php echo(isset($_POST['enviar']) && !empty($errores) ? $asunto : ""); ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label class="label-control">Message</label>
							<textarea class="form-control" name="mensaje"><?php echo(isset($_POST['enviar']) && !empty($errores) ? $mensaje : ""); ?></textarea>
							<button type="submit" class="pull-right btn btn-lg sr-button" name="enviar">SEND</button>
						</div>
					</div>
				</form>
				<hr class="divider">
				<br><br><br>
				<?php if (plantilla\core\Security::isUserGranted('ROLE_ADMIN') === true) { ?>
					<div class="messages">
						<h3>Mensajes</h3>
						<hr>
						<table class="table">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Persona</th>
									<th scope="col">Texto</th>
									<th scope="col">Validado</th>
									<th scope="col">Borrar</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach (($mensajes ?? []) as $mensaje) { ?>
								<tr>
									<th scope="row"><?= $mensaje->getId() ?></th>
									<td><?= $mensaje->getNombre().' '.$mensaje->getApellidos() ?></td>
									<td><?= $mensaje->getTexto()?></td>
									<td>
								<?php if ($mensaje->getValidado()) { ?>
										<div class="alert alert-success" align="center">VALIDADO</div>
								<?php } else { ?>
										<div class="alert alert-danger" align="center">NO VALIDADO</div>
										<form action="/contact/validar" method="POST">
											<input type="text" name="id" value="<?= $mensaje->getId() ?>" hidden>
											<button type="submit" class="btn btn-lg sr-button" name="validar">VALIDAR</button>
										</form>
								<?php } ?>	
									</td>
									<td>
										<form action="/contact/borrar" method="POST">
											<input type="text" name="id" value="<?= $mensaje->getId() ?>" hidden>
											<button type="submit" class="btn btn-block" name="borrar">BORRAR</button>
										</form>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
					<hr class="divider">
				<?php } ?>
				<div class="address">
					<h3>GET IN TOUCH</h3>
					<hr>
					<p>Sunt ut voluptatum eius sapiente, totam reiciendis
						temporibus qui quibusdam, recusandae sit vero.</p>
					<div class="ending text-center">
						<ul class="list-inline social-buttons">
							<li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-twitter sr-icons"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
							</li>
						</ul>
						<ul class="list-inline contact">
							<li class="footer-number"><i class="fa fa-phone sr-icons"></i>
								(00228)92229954</li>
							<li><i class="fa fa-envelope sr-icons"></i>
								kouvenceslas93@gmail.com</li>
						</ul>
						<p>Photography Fanatic Template &copy; 2017</p>
					</div>
				</div>
			</div>
		</div>
	</div>