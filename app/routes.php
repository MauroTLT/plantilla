<?php

$router->get('', 'PagesController@index');
$router->get('about', 'PagesController@about');
$router->get('associats', 'AssociatController@associats', 'ROLE_USER');
$router->get('blog', 'PagesController@blog');

$router->get('contact', 'MessageController@contact');
$router->post('contact/mensaje', 'MessageController@nuevoMensaje');
$router->post('contact/validar', 'MessageController@validarMensaje');
$router->post('contact/borrar', 'MessageController@borrarMensaje');

$router->get('imagenes-galeria', 'ImagenGaleriaController@galeria', 'ROLE_USER');
$router->get('post', 'PagesController@post');

$router->post('imagenes-galeria/nueva', 'ImagenGaleriaController@nuevaImagenGaleria', 'ROLE_ADMIN');
$router->post('nuevo-associat/nuevo', 'AssociatController@nuevoAssociat', 'ROLE_ADMIN');
$router->post('associats/imprimir', 'AssociatController@imprimirAssociats');
$router->post('associats/borrar', 'AssociatController@deleteAssociat');
$router->post('associats/modify', 'AssociatController@modifyAssociat');

$router->get('imagenes-galeria/:id', 'ImagenGaleriaController@show', 'ROLE_USER');
$router->get('associats/:id', 'AssociatController@show');

$router->get('register', 'AuthController@register');
$router->post('check-register', 'AuthController@checkRegister');
$router->get('login', 'AuthController@login');
$router->post('check-login', 'AuthController@checkLogin');
$router->get('logout', 'AuthController@logout', 'ROLE_USER');