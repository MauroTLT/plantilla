<?php 
ini_set('display_errors',1);

use plantilla\app\entity\Associat;
use plantilla\app\exception\FileException;
use plantilla\app\exception\QueryException;
use plantilla\app\exception\AppException;
use plantilla\app\repository\AssociatRepository;
use plantilla\app\utils\File;

try {

	$errores = [];
	$nombre = "";
	$descripcion = "";
	$mensajeConfirmacion = "";

	$imagenes = App::getRepository(AssociatRepository::class)->findAll();

	require_once(__DIR__.'/../views/associats_view.php');

} catch (FileException $fileException) {
	$errores[] = $fileException->getMessage();
} catch (QueryException $queryException) {
	$errores[] = $queryException->getMessage();
} catch (AppException $appException) {
	$errores[] = $appException->getMessage();
}
?>
 