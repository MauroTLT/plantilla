<?php

namespace plantilla\app\controllers;

use plantilla\app\entity\Usuario;
use plantilla\app\repository\UsuarioRepository;
use plantilla\core\helpers\FlashMessage;
use plantilla\core\Response;
use plantilla\core\App;
use Exception;

class AuthController
{

	public function register()
	{
		$errores = FlashMessage::get('register-error', []);
		$username = FlashMessage::get('username');
		Response::renderView('register', 'layout', compact('errores', 'username'));
	}

	public function checkRegister()
	{
		try {
			if (!isset($_POST['username']) || empty($_POST['username'])) {
				throw new Exception("Debes Introducir el usuario y el password");
			}
			FlashMessage::set('username', $_POST['username']);
			if (!isset($_POST['password']) || empty($_POST['password'])) {
				throw new Exception("Debes Introducir el usuario y el password");
			}

			$usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
				'username' => $_POST['username']
			]);

			if (!is_null($usuario)) {
				throw new Exception("El usuario ya existe");
			}

			$usuarioNuevo = new Usuario($_POST['username'], $_POST['password'], "ROLE_USER");

			$usuarioRepository = App::getRepository(UsuarioRepository::class);
			$usuarioRepository->guarda($usuarioNuevo);

			$_SESSION['loguedUser'] = $usuarioNuevo->getId();

			FlashMessage::unset('username');
			App::get('router')->redirect('');
		} catch (Exception $exception) {
			FlashMessage::set('register-error', [$exception->getMessage().$_POST['username']]);

			App::get('router')->redirect('register');
		}
	}

	public function login()
	{
		$errores = FlashMessage::get('login-error', []);
		$username = FlashMessage::get('username');
		Response::renderView('login', 'layout', compact('errores', 'username'));
	}

	public function checkLogin()
	{
		try {
			if (!isset($_POST['username']) || empty($_POST['username'])) {
				throw new Exception("Debes Introducir el usuario y el password");
			}
			FlashMessage::set('username', $_POST['username']);
			if (!isset($_POST['password']) || empty($_POST['password'])) {
				throw new Exception("Debes Introducir el usuario y el password");
			}

			$usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
				'username' => $_POST['username'],
				'password' => $_POST['password']
			]);

			if (!is_null($usuario)) {
				$_SESSION['loguedUser'] = $usuario->getId();

				FlashMessage::unset('username');

				App::get('router')->redirect('');
			}

			throw new Exception("El usuario y el password introducidos no existen");
		} catch (Exception $exception) {
			FlashMessage::set('login-error', [$exception->getMessage()]);

			App::get('router')->redirect('login');
		}
	}

	public function logout()
	{
		if (isset($_SESSION['loguedUser'])) {
			$_SESSION['loguedUser'] = null;
			unset($_SESSION['loguedUser']);
		}
		App::get('router')->redirect('login');
	}

}