<?php 
ini_set('display_errors',1);
use plantilla\app\exceptions\AppException;
use plantilla\app\exceptions\FileException;
use plantilla\app\exceptions\QueryException;
use plantilla\app\repository\ImagenGaleriaRepository;
use plantilla\app\entity\ImagenGaleria;
use plantilla\app\utils\File;
use plantilla\core\App;
	
try {

	$descripcion = trim(htmlspecialchars($_POST['descripcion']));
	$categoria = trim(htmlspecialchars($_POST['categoria']));

	$tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
	$imagen = new File('imagen', $tiposAceptados);
	$imagen->saveUploadFile(ImagenGaleria::RUTA_IMATGES_GALERIA);
	$imagen->copyFile(ImagenGaleria::RUTA_IMATGES_GALERIA, ImagenGaleria::RUTA_IMATGES_PORTFOLIO);

	$imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);

	$imgRepository = App::getRepository(ImagenGaleriaRepository::class);
	$imgRepository->guarda($imagenGaleria);

	$message = "Se ha guardado una nueva imagen: ".$imagenGaleria->getNom();
	App::get('logger')->add($message);
	
	App::get('router')->redirect('imagenes-galeria');

} catch (FileException $fileException) {
	die($fileException->getMessage());
} catch (QueryException $queryException) {
	die($queryException->getMessage());
} catch (AppException $appException) {
	die($appException->getMessage());
}
