<?php 
ini_set('display_errors',1);
use plantilla\app\exceptions\AppException;
use plantilla\app\exceptions\FileException;
use plantilla\app\exceptions\QueryException;
use plantilla\app\repository\ImagenGaleriaRepository;
use plantilla\app\entity\ImagenGaleria;
use plantilla\app\utils\File;
use plantilla\core\App;
	
try {

	$descripcion = trim(htmlspecialchars($_POST['descripcion']));
	$nombre = trim(htmlspecialchars($_POST['nombre']));

	$tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
	$imagen = new File('imagen', $tiposAceptados);
	$imagen->saveUploadFile(Associat::RUTA_IMAGES_LOGOS);

	$imagenAsoc = new Associat($nombre, $imagen->getFileName(), $descripcion);

	$asociadoRepository = App::getRepository(AssociatRepository::class);
	$asociadoRepository->guarda($imagenAsoc);

	$descripcion = '';
	$message = "Se ha guardado una nueva imagen: ".$imagenAsoc->getNom(); //mensajeConfirmacion
	App::get('logger')->add($message);
	
	App::get('router')->redirect('associats');

} catch (FileException $fileException) {
	die($fileException->getMessage());
} catch (QueryException $queryException) {
	die($queryException->getMessage());
} catch (AppException $appException) {
	die($appException->getMessage());
}
