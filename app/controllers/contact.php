<?php

	if (isset($_POST['enviar'])) {
		$error = 0;
		$nombre = htmlspecialchars(trim($_POST['nombre']));
		$apellido = htmlspecialchars(trim($_POST['apellido']));
		$email = htmlspecialchars(trim($_POST['email']));
		$asunto = htmlspecialchars(trim($_POST['asunto']));
		$mensaje = htmlspecialchars(trim($_POST['mensaje']));

		if (empty($nombre) || empty($email) || empty($asunto)) {
			$error = 1;
			echo "<script>alert('Algún campo importante no ha sido rellenado');</script>";
		}
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$error = 1;
			echo "<script>alert('El correo es incorrecto');</script>";
		}
	}

	require_once(__DIR__.'/../views/contact_view.php');
?>
 