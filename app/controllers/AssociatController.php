<?php

namespace plantilla\app\controllers;

use plantilla\app\exception\AppException;
use plantilla\app\exceptions\QueryException;
use plantilla\app\repository\AssociatRepository;
use plantilla\app\repository\CategoriaRepository;
use plantilla\app\repository\ImagenGaleriaRepository;
use plantilla\app\entity\ImagenGaleria;
use plantilla\app\entity\Associat;
use plantilla\app\utils\Utils;
use plantilla\app\utils\File;
use plantilla\core\Response;
use plantilla\core\App;
use Dompdf\Dompdf;
use Exception;

class AssociatController
{

	public function associats()
	{
		$errores = [];
		$nombre = "";
		$descripcion = "";
		$mensajeConfirmacion = "";

		$imagenes = App::getRepository(AssociatRepository::class)->findAll();

		Response::renderView('associats', 'layout', compact('errores', 'nombre', 'descripcion', 'mensajeConfirmacion', 'imagenes'));
	}

	public function nuevoAssociat()
	{
		$descripcion = trim(htmlspecialchars($_POST['descripcion']));
		$nombre = trim(htmlspecialchars($_POST['nombre']));

		$tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
		$imagen = new File('imagen', $tiposAceptados);
		$imagen->saveUploadFile(Associat::RUTA_IMAGES_LOGOS);

		$imagenAsoc = new Associat($nombre, $imagen->getFileName(), $descripcion);

		$asociadoRepository = App::getRepository(AssociatRepository::class);
		$asociadoRepository->guarda($imagenAsoc);

		$descripcion = '';
		$message = "Se ha guardado una nueva imagen: ".$imagenAsoc->getNom(); //mensajeConfirmacion
		App::get('logger')->add($message);
		
		App::get('router')->redirect('associats');
	}

	public function deleteAssociat()
	{
		$id = trim(htmlspecialchars($_POST['id']));

		$asociado = App::getRepository(AssociatRepository::class)->find($id);

		if (is_null($asociado)) {
			throw new Exception("Asociado ".$id." no encontrado");
		}

		$asociadoRepository = App::getRepository(AssociatRepository::class);
		$asociadoRepository->delete($id);

		$message = "Se ha eliminado un asociado: ".$asociado->getNom(); //mensajeConfirmacion
		App::get('logger')->add($message);
		
		App::get('router')->redirect('associats');
	}

	public function modifyAssociat()
	{
		$id = trim(htmlspecialchars($_POST['id']));
		$descripcion = trim(htmlspecialchars($_POST['descripcion']));
		$nombre = trim(htmlspecialchars($_POST['nombre']));

		$tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
		$imagen = new File('imagen', $tiposAceptados);
		$imagen->saveUploadFile(Associat::RUTA_IMAGES_LOGOS);

		$asociado = new Associat($nombre, $imagen->getFileName(), $descripcion);
		$asociado->setId($id);

		$asociadoRepository = App::getRepository(AssociatRepository::class);
		$asociadoRepository->update($asociado);

		$message = "Se ha modificado un asociado: ".$asociado->getNom(); //mensajeConfirmacion
		App::get('logger')->add($message);
		
		App::get('router')->redirect('associats');
	}

	public function imprimirAssociats()
	{
		$imagenes = App::getRepository(AssociatRepository::class)->findAll();
		$html = Response::returnView('lista_asociado', 'layout', compact('imagenes'));
		App::get('pdf')->prints($html);
	}

	public function show(int $id)
	{
		$asociado = App::getRepository(AssociatRepository::class)->find($id);

		Response::renderView('show-asociado', 'layout', compact('asociado'));
	}
}