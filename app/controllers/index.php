<?php 
ini_set('display_errors',1);

use plantilla\app\exceptions\QueryException;
use plantilla\app\repository\AssociatRepository;
use plantilla\app\repository\ImagenGaleriaRepository;
use plantilla\app\utils\Utils;
use plantilla\core\App;

$asociadosTodos = [];
$imagenGaleria = [];
$categorias = ['category1' => 1, 'category2' => 0, 'category3' => 0];

try {
	
	$imagenGaleria = App::getRepository(ImagenGaleriaRepository::class)->findAll();
	$asociadosTodos = App::getRepository(AssociatRepository::class)->findAll();

	$imagenGaleria = Utils::arrayNRandom($imagenGaleria, 12);
	$asociados = Utils::arrayNRandom($asociadosTodos, 3);

	require_once(__DIR__.'/../app/views/index_view.php');
} catch (QueryException $queryException) {
	$errores[] = $queryException->getMessage();
}
?>