<?php

namespace plantilla\app\controllers;

use plantilla\app\exception\AppException;
use plantilla\app\exceptions\QueryException;
use plantilla\app\repository\AssociatRepository;
use plantilla\app\repository\CategoriaRepository;
use plantilla\app\repository\ImagenGaleriaRepository;
use plantilla\app\repository\MensajeRepository;
use plantilla\app\entity\ImagenGaleria;
use plantilla\app\utils\Utils;
use plantilla\app\utils\File;
use plantilla\core\Response;
use plantilla\core\App;

class PagesController
{
	
	public function index()
	{
		$categorias = ['category1' => 1, 'category2' => 0, 'category3' => 0];

		$imagenGaleria = App::getRepository(ImagenGaleriaRepository::class)->findAll();
		$asociados = App::getRepository(AssociatRepository::class)->findAll();

		$imagenGaleria = Utils::arrayNRandom($imagenGaleria, 12);
		$asociados = Utils::arrayNRandom($asociados, 3);

		Response::renderView('index', 'layout', compact('categorias', 'imagenGaleria', 'asociados'));
	}

	public function about()
	{
		$mensajes = App::getRepository(MensajeRepository::class)->findBy([
			'validado' => 1
		]);
		shuffle($mensajes);
		$i = 0;
		Response::renderView('about', 'layout-with-footer', compact('mensajes', 'i'));
	}

	public function blog()
	{
		Response::renderView('blog', 'layout-with-footer');
	}

	public function post()
	{
		Response::renderView('single_post', 'layout-with-footer');
	}
}