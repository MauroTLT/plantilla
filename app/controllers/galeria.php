<?php 
ini_set('display_errors',1);

use plantilla\app\exception\AppException;
use plantilla\app\exception\QueryException;
use plantilla\app\repository\CategoriaRepository;
use plantilla\app\repository\ImagenGaleriaRepository;
use plantilla\core\App;
	
try {
	$errores = [];
	$descripcion = "";
	$mensajeConfirmacion = "";
	$imagenes = [];
	
	$imagenes = App::getRepository(ImagenGaleriaRepository::class)->findAll();
	$categorias = App::getRepository(CategoriaRepository::class)->findAll();

	require_once(__DIR__.'/../views/galeria_view.php');

} catch (QueryException $queryException) {
	$errores[] = $queryException->getMessage();
} catch (AppException $appException) {
	$errores[] = $appException->getMessage();
}
?>
 