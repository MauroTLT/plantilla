<?php

namespace plantilla\app\controllers;

use plantilla\app\exception\AppException;
use plantilla\app\exceptions\QueryException;
use plantilla\app\exceptions\FileException;
use plantilla\app\repository\AssociatRepository;
use plantilla\app\repository\CategoriaRepository;
use plantilla\app\repository\ImagenGaleriaRepository;
use plantilla\app\entity\ImagenGaleria;
use plantilla\app\utils\Utils;
use plantilla\app\utils\File;
use plantilla\core\helpers\FlashMessage;
use plantilla\core\Response;
use plantilla\core\App;

class ImagenGaleriaController
{

	public function galeria()
	{
		$imagenes = [];
		$errores = FlashMessage::get('errores', []);
		$descripcion = FlashMessage::get('descripcion');
		$mensajeConfirmacion = FlashMessage::get('mensaje');
		$categoriaSeleccionada = FlashMessage::get('categoriaSeleccionada');
		
		$imagenes = App::getRepository(ImagenGaleriaRepository::class)->findAll();
		$categorias = App::getRepository(CategoriaRepository::class)->findAll();

		Response::renderView('galeria', 'layout', compact('errores', 'descripcion', 'mensajeConfirmacion', 'imagenes', 'categorias', 'categoriaSeleccionada'));
	}

	public function nuevaImagenGaleria()
	{
		try {
			$descripcion = trim(htmlspecialchars($_POST['descripcion']));
			FlashMessage::set('descripcion', $descripcion);
			$categoria = trim(htmlspecialchars($_POST['categoria']));
			FlashMessage::set('categoriaSeleccionada', $categoria);

			$tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
			$imagen = new File('imagen', $tiposAceptados);
			$imagen->saveUploadFile(ImagenGaleria::RUTA_IMATGES_GALERIA);
			$imagen->copyFile(ImagenGaleria::RUTA_IMATGES_GALERIA, ImagenGaleria::RUTA_IMATGES_PORTFOLIO);

			$imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);

			$imgRepository = App::getRepository(ImagenGaleriaRepository::class);
			$imgRepository->guarda($imagenGaleria);

			$message = "Se ha guardado una nueva imagen: ".$imagenGaleria->getNom();
			App::get('logger')->add($message);

			FlashMessage::set('mensaje', $message);
			FlashMessage::unset('descripcion');
			FlashMessage::unset('categoriaSeleccionada');

		} catch (FileException $fileException) {
			FlashMessage::set('errores', [$fileException->getMessage()]);
		}
		App::get('router')->redirect('imagenes-galeria');
	}

	public function show(int $id)
	{
		$imagen = App::getRepository(ImagenGaleriaRepository::class)->find($id);
		$filtros = [
			'Negativo' => [IMG_FILTER_NEGATE, 0, 0, 0],
			'GrayScale' => [IMG_FILTER_GRAYSCALE, 0, 0, 0],
			'Pixelate' => [IMG_FILTER_PIXELATE, rand(2, 20), 0],
			'EdgeDetect' => [IMG_FILTER_EDGEDETECT, 0, 0, 0],
			'Emboss' => [IMG_FILTER_EMBOSS, 0, 0, 0],
			'Selective Blur' => [IMG_FILTER_SELECTIVE_BLUR, 0, 0, 0],
			'Gaussian Blur' => [IMG_FILTER_GAUSSIAN_BLUR, 0, 0, 0],
			'Sketchy' => [IMG_FILTER_MEAN_REMOVAL, 0, 0, 0],
			'Coloreado' => [IMG_FILTER_COLORIZE, rand(0,255), rand(0,255), rand(0,255)]
		];
		$filtro = null;
		if (isset($_GET['efecto'])) {
			$filtro = $_GET['efecto'];

			header('Content-Type: image/jpeg');
			if (strstr($imagen->getUrlGallery(), '.') == ".png") {
				$imagen2 = imagecreatefrompng($imagen->getUrlGallery());
			} else if (strstr($imagen->getUrlGallery(), '.') == ".gif") {
				$imagen2 = imagecreatefromgif($imagen->getUrlGallery());
			} else {
				$imagen2 = imagecreatefromjpeg($imagen->getUrlGallery());
			}
			foreach ($filtro as $fil) {
				if ($fil == 'Pixelate') {
					imagefilter($imagen2, $filtros[$fil][0],
						$filtros[$fil][1],
						$filtros[$fil][2]
					);
				} else {
					imagefilter($imagen2, $filtros[$fil][0],
						$filtros[$fil][1],
						$filtros[$fil][2],
						$filtros[$fil][3]
					);
				}
				
			}
			imagepng($imagen2);
			imagedestroy($imagen2);
		}

		Response::renderView('show-imagen-galeria', 'layout', compact('imagen', 'imagen2', 'filtros', 'filtro'));
	}
}