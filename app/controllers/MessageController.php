<?php

namespace plantilla\app\controllers;

use plantilla\app\exception\AppException;
use plantilla\app\exceptions\QueryException;
use plantilla\app\repository\MensajeRepository;
use plantilla\app\entity\Mensaje;
use plantilla\app\utils\Utils;
use plantilla\app\utils\File;
use plantilla\core\helpers\FlashMessage;
use plantilla\core\Response;
use plantilla\core\App;
use Exception;

class MessageController
{

	public function contact()
	{
		$errores = FlashMessage::get('errores', []);
		$nombre = FlashMessage::get('nombre');
		$apellido = FlashMessage::get('apellido');
		$email = FlashMessage::get('email');
		$asunto = FlashMessage::get('asunto');
		$mensaje = FlashMessage::get('mensaje');

		$mensajes = App::getRepository(MensajeRepository::class)->findAll();

		Response::renderView('contact', 'layout', compact('errores', 'nombre', 'apellido', 'email', 'asunto', 'mensaje', 'mensajes'));
	}

	public function nuevoMensaje()
	{
		try {
			$nombre = htmlspecialchars(trim($_POST['nombre']));
			FlashMessage::set('nombre', $nombre);
			$apellido = htmlspecialchars(trim($_POST['apellido']));
			FlashMessage::set('apellido', $apellido);
			$email = htmlspecialchars(trim($_POST['email']));
			FlashMessage::set('email', $email);
			$asunto = htmlspecialchars(trim($_POST['asunto']));
			FlashMessage::set('asunto', $asunto);
			$mensaje = htmlspecialchars(trim($_POST['mensaje']));
			FlashMessage::set('mensaje', $mensaje);

			if (empty($nombre) || empty($email) || empty($asunto)) {
				throw new Exception('Algún campo importante no ha sido rellenado');
			}
			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				throw new Exception('El correo es incorrecto');
			}

			$mensajeContact = new Mensaje($nombre, $apellido, $asunto, $email, $mensaje);

			$mensajeRepository = App::getRepository(MensajeRepository::class);
			$mensajeRepository->guarda($mensajeContact);

			$message = "Se ha guardado un nuevo Mensaje: ".$mensaje;
			App::get('logger')->add($message);

			//App::get('mailer')->send($asunto, $email, $nombre, $mensaje);
		} catch (Exception $messageException) {
			FlashMessage::set('errores', [$messageException->getMessage()]);
		}
		App::get('router')->redirect('contact');
	}

	public function validarMensaje() {
		$id = trim(htmlspecialchars($_POST['id']));

		$mensaje = App::getRepository(MensajeRepository::class)->find($id);
		$mensaje->setValidado(1);

		$mensajeRepository = App::getRepository(MensajeRepository::class);
		$mensajeRepository->update($mensaje);

		$message = "Se ha validado un mensaje: ".$mensaje->getId();
		App::get('logger')->add($message);

		App::get('router')->redirect('contact');
	}

	public function borrarMensaje() {
		$id = trim(htmlspecialchars($_POST['id']));

		$mensaje = App::getRepository(MensajeRepository::class)->find($id);

		if (is_null($mensaje)) {
			throw new Exception("Mensaje ".$id." no encontrado");
		}

		$mensajeRepository = App::getRepository(MensajeRepository::class);
		$mensajeRepository->delete($id);

		$message = "Se ha borrado un mensaje: ".$mensaje->getId();
		App::get('logger')->add($message);

		App::get('router')->redirect('contact');
	}
}