<?php

namespace plantilla\app\exceptions;

use Exception;

class QueryException extends AppException
{
	public function __construct($message, $code = 500)
	{
		parent::__construct($message, $code);
	}
}