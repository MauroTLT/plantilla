<?php

namespace plantilla\app\exceptions;

use plantilla\core\Response;
use Exception;

class AuthenticationException extends AppException
{
	
	public function __construct($message, $code = 403)
	{
		parent::__construct($message, $code);
	}
}