<?php

namespace plantilla\app\exceptions;

use Exception;

class FileException extends Exception
{
	
	function __construct(string $message)
	{
		parent::__construct($message);
	}
}