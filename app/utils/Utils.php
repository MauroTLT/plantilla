<?php

namespace plantilla\app\utils;

class Utils
{
	public static function opcionActiva($pagina){
		$actual = $_SERVER['REQUEST_URI'];
		if (strpos($actual, $pagina)) {
			return "active";
		} else {
			return "lien";
		}
	}

	public static function arrayNRandom($array, $num) {
		shuffle($array);
		return array_chunk($array, $num)[0];
	}

	public static function calculateWidth($mensajes, $i)
	{
		if (sizeof($mensajes) > ($i+sizeof($mensajes)%4)) {
			return '3';
		} else if (sizeof($mensajes)%4 == 1){
			return '12';
		} else if (sizeof($mensajes)%4 == 2){
			return '6';
		} else if (sizeof($mensajes)%4 == 3){
			return '4';
		} else {
			return '1';
		}
	}
}
?>