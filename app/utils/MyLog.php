<?php

namespace plantilla\app\utils;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MyLog
{
	private $log;
	private $level;
	
	private function __construct($filename, $level)
	{
		$this->level = $level;
		$this->log = new Logger('name');
		$this->log->pushHandler(new StreamHandler($filename, $this->level));
	}

	public function load($filename, $level = Logger::INFO)
	{
		return new MyLog($filename, $level);
	}

	public function add($message)
	{
		$this->log->log($this->level, $message);
	}
}