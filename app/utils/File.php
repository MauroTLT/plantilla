<?php

namespace plantilla\app\utils;

use plantilla\app\exceptions\FileException;

class File {

    private $file;
    private $fileName;

    public function __construct($fileName, $tiposPerm){
        $this->file = $_FILES[$fileName];
        $this->fileName = "";

        if (!isset($this->file)) { 
            throw new FileException("Debes seleccionar un fichero");
        }
        if ($this->file['error'] !== UPLOAD_ERR_OK) {
            switch ($this->file['error']) {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new FileException("El fichero es demasiado grande");
                    break;
                case UPLOAD_ERR_PARTIAL:
                    throw new FileException("No se ha podido subir el fichero completo");
                    break;
                default:
                    throw new FileException("No se ha podido subir el fichero");
                    break;
            }
        }

        if (in_array($this->file['type'], $tiposPerm) === false) {
            throw new FileException("La extensión del fichero no esta soportada"); 
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function getFileName() {
        return $this->fileName;
    }

    public function setFileName($fileName) {
        $this->file = $fileName;
    }

    public function saveUploadFile($rutaDestino) {
        if (is_uploaded_file($this->file['tmp_name']) === false) {
            throw new FileException("El archivo no ha sido subido mediante un formulario");
        }
        $this->fileName = $this->file['name'];
        $ruta = $rutaDestino.$this->fileName;

        if (is_file($ruta) === true) {
            $idUnico = time();
            $this->fileName = $idUnico.'_'.$this->fileName;
            $ruta = $rutaDestino.$this->fileName;
        }
        if (!move_uploaded_file($this->file['tmp_name'], $ruta)) {
            throw new FileException("No se puede mover el fichero a su destino");
            
        }
    }

    public function copyFile($rutaOrigen, $rutaDestino) {
        $origen = $rutaOrigen.$this->fileName;
        $destino = $rutaDestino.$this->fileName;

        if (is_file($origen) === false) {
            throw new FileException("No existe el fichero que estas intentando copiar");
        }
        if (is_file($destino) === true) {
            throw new FileException("El fichero ya existe y no se puede sobrescribir");
        }
        if (copy($origen, $destino) === false) {
            throw new FileException("No se ha podido copiar el fichero");
        }
    }

}
