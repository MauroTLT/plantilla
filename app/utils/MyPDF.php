<?php

namespace plantilla\app\utils;

use Dompdf\Dompdf;

class MyPDF
{
	private $dmpdf;
	
	private function __construct()
	{
		$this->dompdf = new Dompdf();
		$this->dompdf->setPaper('A4', 'portrait');
	}

	public function load()
	{
		return new MyPDF();
	}

	public function prints($html)
	{
		$this->dompdf->loadHtml($html);
		$this->dompdf->render();
		$this->dompdf->stream();
	}
}