<?php

namespace plantilla\app\utils;


class MyMail
{
	private $mailer;
	private $email;
	private $name;
	
	private function __construct($config)
	{
		$transport = (new \Swift_SmtpTransport($config['smtp_server'], $config['smtp_port'], $config['smtp_security']))->setUsername('your username')->setPassword('your password');

		$this->mailer = new \Swift_Mailer($transport);
		$this->email = $config['email'];
		$this->name = $config['name'];
	}

	public function load($config)
	{
		return new MyMail($config);
	}

	public function send($asunto, $mailTo, $nameTo, $text)
	{
		$message = (new \Swift_Message($asunto))
		  ->setFrom([$this->email => $this->name])
		  ->setTo([$mailTo => $nameTo])
		  ->setBody($text);

		$this->mailer->send($message);
	}
}