<?php
ini_set('display_errors', 1);

use plantilla\app\exceptions\AppException;
use plantilla\app\exceptions\NotFoundException;
use plantilla\core\App;
use plantilla\core\Request;

try {
	require __DIR__.'/../core/bootstrap.php';

	App::get('router')->direct(Request::uri(), Request::method());

} catch (AppException $appException) {
	$appException->handleError();
}
