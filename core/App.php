<?php
namespace plantilla\core;

use plantilla\core\database\Connection;
use plantilla\app\exceptions\AppException;

class App
{
	private static $container = [];

	public static function bind($key, $value)
	{
		static::$container[$key] = $value;
	}

	public static function get($key)
	{
		if (!array_key_exists($key, static::$container)) {
			throw new AppException("No se ha encontrado la clave en el contenedor");
		}
		return static::$container[$key];
	}

	public static function getConnection()
	{
		if (!array_key_exists('connection', static::$container)) {
			$config = require_once __DIR__.'/../app/config.php';
			static::$container['connection'] = Connection::make($config);
		}
		return static::$container['connection'];
	}

	public static function getRepository($className)
	{
		if (! array_key_exists($className, static::$container)) {
			static::$container[$className] = new $className();
		}
		return static::$container[$className];
	}
}