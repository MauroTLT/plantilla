<?php

namespace plantilla\core\database;

interface IEntity
{
	public function toArray();
}