<?php

namespace plantilla\core\database;

use plantilla\core\App;
use plantilla\app\exceptions\AppException;
use PDOException;
use PDO;

class Connection
{
	
	public static function make($config) {
		try {
			$config = App::get('config')['database'];
			$connection = new PDO($config['connection'].';dbname='.$config['name'], $config['user'], $config['password'], $config['options']);
		} catch(PDOException $PDOException) {
			throw new AppException('No se ha creado la conexion a la base de datos');
		}

		return $connection;
	}
}