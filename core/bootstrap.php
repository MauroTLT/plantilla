<?php
require __DIR__.'/../vendor/autoload.php';

use plantilla\app\repository\UsuarioRepository;
use plantilla\app\utils\MyLog;
use plantilla\app\utils\MyMail;
use plantilla\app\utils\MyPDF;
use plantilla\core\App;
use plantilla\core\Router;

session_start();

$config = require_once __DIR__.'/../app/config.php';
App::bind('config', $config);

$router = Router::load(__DIR__.'/../app/'.$config['routes']['filename']);
App::bind('router', $router);

$logger = MyLog::load(__DIR__.'/../logs/'.$config['logs']['filename'], $config['logs']['level']);
App::bind('logger', $logger);

$mailer = MyMail::load($config['swiftmail']);
App::bind('mailer', $mailer);

$pdf = MyPDF::load();
App::bind('pdf', $pdf);

if (isset($_SESSION['loguedUser'])) {
	$appUser = App::getRepository(UsuarioRepository::class)->find($_SESSION['loguedUser']);
} else {
	$appUser = null;
}

App::bind('appUser', $appUser);