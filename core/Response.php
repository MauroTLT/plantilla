<?php

namespace plantilla\core;

use plantilla\core\App;

class Response
{
	public static function renderView($name, $layout='layout', $data = [])
	{
		extract($data);

		$app['user'] = App::get('appUser');

		ob_start();

		require __DIR__.'/../app/views/'.$name.'_view.php';

		$mainContent = ob_get_clean();
		
		require __DIR__.'/../app/views/'.$layout.'.view.php';
	}

	public static function returnView($name, $layout='layout', $data = [])
	{
		extract($data);

		$app['user'] = App::get('appUser');

		ob_start();

		require __DIR__.'/../app/views/'.$name.'_view.php';

		$mainContent = ob_get_clean();

		return $mainContent;
	}
}